This is the repository for all profiles for TICS Pro versions >=2.0. This 
repository is mirrored to git.ti.com. TICS Pro versions >=2.0 can check the 
manifest file in this repository to determine if profile updates are available.

Currently this repository is under development and only a test profile is 
present. This test profile is not intended for use outside of internal 
development. Updated profiles will be available in the near future.