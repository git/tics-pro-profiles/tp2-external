def FlexTest3_Update():
	if FlexTest3.iValue == 0: FlexTest1.sValue = "hello"
	elif FlexTest3.iValue == 1: FlexTest1.sValue = "world"
	elif FlexTest3.iValue == 2: FlexTest1.sValue = "this is"
	else: FlexTest1.sValue = "a test"

def ProfileVersion_Update(path):
	import ConfigParser
	cp = ConfigParser.RawConfigParser()
	cp.optionxform = str
	cp.read(path)
	changed = (not cp.has_option("SETUP", "VERSION")) or (cp.getint("SETUP","VERSION") < 4)
	if changed:
		cp.set("SETUP", "VERSION", "4")
		val = cp.get("PARAMETERS","FlexTest2")
		cp.set("PARAMETERS","FlexTest3",val)
		cp.remove_option("PARAMETERS","FlexTest2")
		with open(path, 'wb') as f:
			cp.write(f)

def btnTestVersion_Update():
	ProfileVersion_Update("C:\\Users\\a0227372\\Documents\\test.tcs")
